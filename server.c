/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: azhadan <azhadan@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/05/30 16:41:56 by azhadan           #+#    #+#             */
/*   Updated: 2023/06/03 01:29:11 by azhadan          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minitalk.h"

typedef struct all_needed_numbers
{
	char				ascii;
	int					bit;
}						t_all_needed_numbers;

t_all_needed_numbers	*numbers;

void	ft_print_bytes(int sig)
{
	if (!numbers->bit)
		numbers->bit = 128;
	if (sig == SIGUSR2)
		numbers->ascii += numbers->bit;
	if (numbers->bit == 1)
	{
		ft_printf("%c", numbers->ascii);
		numbers->bit = 128;
		numbers->ascii = 0;
		return ;
	}
	numbers->bit /= 2;
}

void	ft_clean(int sig)
{
	(void)sig;
	free(numbers);
	exit(0);
}

int	main(void)
{
	numbers = (t_all_needed_numbers *)malloc(sizeof(t_all_needed_numbers));
	numbers->bit = 0;
	numbers->ascii = 0;
	ft_printf("Pid: %d\n", getpid());
	signal(SIGUSR1, ft_print_bytes);
	signal(SIGUSR2, ft_print_bytes);
	signal(SIGTERM, ft_clean);
	while (1)
		sleep(1);
	return (0);
}
