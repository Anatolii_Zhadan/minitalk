# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: azhadan <azhadan@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2023/05/29 20:30:06 by azhadan           #+#    #+#              #
#    Updated: 2023/05/31 02:26:38 by azhadan          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SERVER := server
CLIENT := client
NAME := server
SERVER_SRC := server.c 
CLIENT_SRC := client.c
LIB := libft/libft.a
SERVER_OBJ := $(SERVER_SRC:.c=.o)
CLIENT_OBJ := $(CLIENT_SRC:.c=.o)

CFLAGS := -Wall -Wextra -Werror

all: $(NAME)

$(NAME): library server client

library:
	@cd libft && make

server: $(SERVER_OBJ)
	@cc $(SERVER_OBJ) $(LIB) -o $(SERVER)

client: $(CLIENT_OBJ)
	@cc $(CLIENT_OBJ) $(LIB) -o $(CLIENT)

clean:
	@rm -rf $(CLIENT_OBJ)
	@rm -rf $(SERVER_OBJ)
	@cd libft && make clean

fclean: clean
	@rm -rf $(SERVER) $(CLIENT)
	@cd libft && make fclean

.c.o:
	@cc $(CFLAGS) -c $< -o $@

re: fclean all

.PHONY: all library server client clean fclean re
